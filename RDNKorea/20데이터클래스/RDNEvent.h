//
//  RDNEvent.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDNEvent : NSObject
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *route;
@property (nonatomic, copy) NSString *thumbnail;
@end
