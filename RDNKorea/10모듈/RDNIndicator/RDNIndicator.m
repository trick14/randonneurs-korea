//
//  RDNIndicator.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "RDNIndicator.h"
#define kFadeAnimationDuration 0.3

@interface RDNIndicator()
{
    UIView *deem_;
    UIActivityIndicatorView *indicator_;
}
@property (nonatomic, assign) NSInteger count;
@end

@implementation RDNIndicator
+ (void)increase {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if([RDNIndicator shared].count == 0) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [[RDNIndicator shared] show];
            });
        }
        [RDNIndicator shared].count++;
    });
}

+ (void)decrease {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [RDNIndicator shared].count = MAX(0, [RDNIndicator shared].count - 1);
        if([RDNIndicator shared].count == 0) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                [[RDNIndicator shared] hide];
            });
        }
    });
}

+ (RDNIndicator *)shared {
    static RDNIndicator *indicator;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        indicator = [[RDNIndicator alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    });
    return indicator;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
        
        deem_ = [[UIView alloc] initWithFrame:frame];
        [deem_ setBackgroundColor:[UIColor lightGrayColor]];
        [deem_ setAlpha:0.6];
        [self addSubview:deem_];
        
        indicator_ = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicator_ setCenter:self.center];
        [self addSubview:indicator_];
    }
    return self;
}

- (void)show {
    [self setAlpha:0];
    [indicator_ startAnimating];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    [UIView animateWithDuration:kFadeAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self setAlpha:1];
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)hide {
    [UIView animateWithDuration:kFadeAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self setAlpha:0];
                     } completion:^(BOOL finished) {
                         [indicator_ stopAnimating];
                         [self removeFromSuperview];
                     }];
}
@end
