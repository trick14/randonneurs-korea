//
//  AppDelegate.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "MenuViewController.h"
#import "RDNViewController.h"

@interface AppDelegate ()
{
    
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Parse setApplicationId:@"Xb6fhIHdoPST0y5UZRNkPyWEjqqL4AgKO3w2WtrR"
                  clientKey:@"elvuy5yg1sPsufNTmHRQ448e80UxgrBNynJBB6FD"];
    
    UINavigationController *nv = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    MenuViewController *mv = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MenuViewController"];
    
    self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:nv leftDrawerViewController:mv];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeBezelPanningCenterView];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [self.drawerController setShowsShadow:YES];
    [self.drawerController setMaximumLeftDrawerWidth:260];
    [self.drawerController setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
        UIViewController * sideDrawerViewController;
        if(drawerSide == MMDrawerSideLeft){
            sideDrawerViewController = drawerController.leftDrawerViewController;
        }
        CGFloat offsetX = -260 + (260 * percentVisible);
        sideDrawerViewController.view.transform = CGAffineTransformMakeTranslation(offsetX, 0);
        
        [(RDNViewController *)nv.topViewController setAlpha:percentVisible];
    }];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window setRootViewController:self.drawerController];
    [self.window makeKeyAndVisible];
    
    if(![RDNUser shared].isSigned && [RDNUser shared].isIdPsswordExist) {
        [RDNRequestManager requestSigninWithId:[RDNUser shared].clienId
                                      password:[RDNUser shared].clienPassword
                                          done:^(BOOL success, id data, id extra) {
                                              if(success) {
                                                  // 로그인 성공
                                                  BKLog(@"로그인 성공");
                                              }
                                              else {
                                                  
                                              }
                                          }];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
