//
//  UIView+BK.m
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 WAG. All rights reserved.
//

#import "UIView+BK.h"

@implementation UIView (BK)
- (CGFloat)frameX
{
    return self.frame.origin.x;
}

- (void)setFrameX:(CGFloat)frameX
{
    CGRect frame_ = self.frame;
    frame_.origin.x = frameX;
    self.frame = frame_;
}

- (CGFloat)frameY
{
    return self.frame.origin.y;
}

- (void)setFrameY:(CGFloat)frameY
{
    CGRect frame_ = self.frame;
    frame_.origin.y = frameY;
    self.frame = frame_;
}

- (CGFloat)frameWidth
{
    return self.frame.size.width;
}

- (void)setFrameWidth:(CGFloat)frameWidth
{
    CGRect frame_ = self.frame;
    frame_.size.width = frameWidth;
    self.frame = frame_;
}

- (CGFloat)frameHeight
{
    return self.frame.size.height;
}

- (void)setFrameHeight:(CGFloat)frameHeight
{
    CGRect frame_ = self.frame;
    frame_.size.height = frameHeight;
    self.frame = frame_;
}

- (CGFloat)frameBottom
{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)frameRight
{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGPoint)frameOrigin
{
    return self.frame.origin;
}

- (void)setFrameOrigin:(CGPoint)frameOrigin
{
    CGRect frame_ = self.frame;
    frame_.origin = frameOrigin;
    self.frame = frame_;
}

- (CGSize)frameSize
{
    return self.frame.size;
}

- (void)setFrameSize:(CGSize)frameSize
{
    CGRect frame_ = self.frame;
    frame_.size = frameSize;
    self.frame = frame_;
}

- (CGFloat)frameCenterX
{
    return self.frame.origin.x + (self.frame.size.width/2);
}

- (void)setFrameCenterX:(CGFloat)frameCenterX
{
    CGRect frame_ = self.frame;
    frame_.origin.x = frameCenterX - (self.frame.size.width/2);
    self.frame = frame_;
}

- (CGFloat)frameCenterY
{
    return self.frame.origin.y + (self.frame.size.height/2);
}

- (void)setFrameCenterY:(CGFloat)frameCenterY
{
    CGRect frame_ = self.frame;
    frame_.origin.y = frameCenterY - (self.frame.size.height/2);
    self.frame = frame_;
}

- (void)setScaleX:(CGFloat)x Y:(CGFloat)y
{
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, x, y);
}

/**
 * 뷰 회전
 * @param round 회전수(바퀴)
 * @param speed 속도(1~10, 클수록 빠름)
 * @return CGFloat 최종 각도(degree)
 */
- (CGFloat)rotationWithRound:(CGFloat)round speed:(NSInteger)speed
{
    CABasicAnimation *animation = (CABasicAnimation *)[self.layer animationForKey:@"rotationAnimation"];
    NSNumber *toValue = [NSNumber numberWithFloat: (M_PI * 2 * round) + [animation.toValue floatValue]];
    CGFloat duration = round / speed;
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue     = animation.toValue;    // 회전할때마다 누적되도록 fromValue를 이전 toValue로 설정
    rotationAnimation.toValue       = toValue;              // 새로 회전되는 toValue
    rotationAnimation.duration      = duration;             // 걸리는 시간
    rotationAnimation.cumulative    = YES;                  // repeat할 때 누적되도록
    rotationAnimation.autoreverses  = NO;                   // 자동 원복 끄기
    rotationAnimation.removedOnCompletion = NO;             // 애니메이션 종료시 애니메이션 오브젝트 Layer에 남도록
    rotationAnimation.fillMode      = kCAFillModeForwards;  //
    rotationAnimation.additive      = YES;                  // 이전 애니메이션에 새로운 애니메이션 추가
    rotationAnimation.repeatCount   = 1;
    
    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    CGFloat degree = [toValue floatValue] * 180.0 / M_PI;
    NSInteger divider = degree/360;
    CGFloat finalDegree = degree - (divider * 360.0);
    
    return finalDegree;
}

- (void)startRotating {
    CABasicAnimation *animation = (CABasicAnimation *)[self.layer animationForKey:@"infinite_rotating"];
    NSNumber *toValue = [NSNumber numberWithFloat: (M_PI * 2) + [animation.toValue floatValue]];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue     = animation.toValue;    // 회전할때마다 누적되도록 fromValue를 이전 toValue로 설정
    rotationAnimation.toValue       = toValue;              // 새로 회전되는 toValue
    rotationAnimation.duration      = 1.0f;                 // 걸리는 시간
    rotationAnimation.cumulative    = YES;                  // repeat할 때 누적되도록
    rotationAnimation.autoreverses  = NO;                   // 자동 원복 끄기
    rotationAnimation.removedOnCompletion = YES;            // 애니메이션 종료시 애니메이션 오브젝트 Layer에 남도록
    rotationAnimation.fillMode      = kCAFillModeForwards;  //
    rotationAnimation.additive      = YES;                  // 이전 애니메이션에 새로운 애니메이션 추가
    rotationAnimation.repeatCount   = FLT_MAX;
    
    [self.layer addAnimation:rotationAnimation forKey:@"infinite_rotating"];
}

- (void)stopRotating {
    [self.layer removeAnimationForKey:@"infinite_rotating"];
}

- (void)sizeToFitForFlexibleHeight
{
    CGSize bestFit_ = [self sizeThatFits:CGSizeMake(self.frameWidth, 0)];
    self.frameHeight = bestFit_.height;
    
}

- (void)sizeToFitForFlexibleWidth
{
    CGSize bestFit_ = [self sizeThatFits:CGSizeMake(0, self.frameHeight)];
    self.frameWidth = bestFit_.width;
}

#pragma mark - 뷰 영역 표시
/*
 *  view        테두리 표시할 뷰
 *  color       테두리 컬러
 *  isInfoShow  view의 0,0에 좌표 및 크기정보 표시
 */
- (void)showViewBorder
{
    [self showViewBorderWithColor:[UIColor redColor]];
}

- (void)showViewBorderWithInfo:(BOOL)isInfoShow;
{
    [self showViewBorderWithColor:[UIColor redColor] info:YES];
}

- (void)showViewBorderWithColor:(UIColor *)color;
{
    [self showViewBorderWithColor:color info:NO];
}

- (void)showViewBorderWithColor:(UIColor *)color info:(BOOL)isInfoShow;
{
    [self.layer setBorderWidth:1.0];
    
    UIColor * color_ = color;
    if(color_ == nil)
        color_ = [UIColor redColor];
    [self.layer setBorderColor:color_.CGColor];
    
    if(isInfoShow)
    {
        UILabel *labelView_ = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 10)];
        [labelView_ setText:NSStringFromCGRect(self.frame)];
        [labelView_ setFont:[UIFont systemFontOfSize:8]];
        [labelView_ setTextAlignment:NSTextAlignmentCenter];
        [labelView_ setTextColor:[UIColor blackColor]];
        [labelView_ setBackgroundColor:color];
        [labelView_ setAdjustsFontSizeToFitWidth:YES];
        [labelView_ setAlpha:0.7];
        [self addSubview:labelView_];
    }
}

- (void)showViewBorderOfSubviews;
{
    for(UIView *view_ in self.subviews) {
        [view_ showViewBorder];
    }
}

- (void)showViewBorderOfSubviewsWithInfo:(BOOL)isInfoShow;
{
    for(UIView *view_ in self.subviews) {
        [view_ showViewBorderWithInfo:isInfoShow];
    }
}
@end
