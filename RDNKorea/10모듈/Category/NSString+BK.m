//
//  NSString+BK.m
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import "NSString+BK.h"

@implementation NSString (BK)

- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options
{
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string
{
    return [self containsString:string options:0];
}

- (NSString *)urlEndoded
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)self,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
}

- (NSString *)urlDecoded
{
    return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
