//
//  UIButton+BK.m
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import "UIButton+BK.h"

@implementation UIButton (BK)

- (void)setImageForNormal:(UIImage *)normalImage forSelected:(UIImage *)selectedImage
{
	[self setImage:normalImage forState:UIControlStateNormal];
	[self setImage:normalImage forState:UIControlStateNormal|UIControlStateHighlighted];
	
	[self setImage:selectedImage forState:UIControlStateSelected];
	[self setImage:selectedImage forState:UIControlStateSelected|UIControlStateHighlighted];
}

- (void)setImageForNormal:(UIImage *)normalImage forHighlighted:(UIImage *)highlightedImage
{
    [self setImage:normalImage forState:UIControlStateNormal];
	[self setImage:highlightedImage forState:UIControlStateHighlighted];
}

- (void)setBGImageForNormal:(UIImage *)normalImage forSelected:(UIImage *)selectedImage
{
	[self setBackgroundImage:normalImage forState:UIControlStateNormal];
	[self setBackgroundImage:normalImage forState:UIControlStateNormal|UIControlStateHighlighted];
	
	[self setBackgroundImage:selectedImage forState:UIControlStateSelected];
	[self setBackgroundImage:selectedImage forState:UIControlStateSelected|UIControlStateHighlighted];
}

- (void)setTitleColorForNormal:(UIColor *)normalColor forSelected:(UIColor *)selectedColor;
{
	[self setTitleColor:normalColor forState:UIControlStateNormal];
	[self setTitleColor:normalColor forState:UIControlStateNormal|UIControlStateHighlighted];
	
	[self setTitleColor:selectedColor forState:UIControlStateSelected];
	[self setTitleColor:selectedColor forState:UIControlStateSelected|UIControlStateHighlighted];
}

- (void)setTitleForNormal:(NSString *)normalTitle forSelected:(NSString *)selectedTitle
{
	[self setTitle:normalTitle forState:UIControlStateNormal];
	[self setTitle:normalTitle forState:UIControlStateNormal|UIControlStateHighlighted];
	
	[self setTitle:selectedTitle forState:UIControlStateSelected];
	[self setTitle:selectedTitle forState:UIControlStateSelected|UIControlStateHighlighted];
}

- (void)setBGImageForNormal:(UIImage *)normalImage forHighlighted:(UIImage *)highlightedImage
{
    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
}

- (void)adjustsWidth
{
	NSString	*title_	= [self titleForState:UIControlStateNormal];
	UIImage		*image_	= [self imageForState:UIControlStateNormal];
    CGFloat     width_  = 0;
    if([kVersionOS intValue] >= 7)
    {
        width_ = [title_ boundingRectWithSize:(CGSize){CGFLOAT_MAX, self.frameHeight} options:NSStringDrawingUsesDeviceMetrics attributes:@{NSFontAttributeName: self.titleLabel.font} context:nil].size.width;
    }
    else
    {
        width_ = [title_ sizeWithFont:self.titleLabel.font forWidth:CGFLOAT_MAX lineBreakMode:self.titleLabel.lineBreakMode].width;
    }
    
    self.frameWidth	= width_ + image_.size.width + self.imageEdgeInsets.left + self.imageEdgeInsets.right + self.titleEdgeInsets.left + self.titleEdgeInsets.right+ self.contentEdgeInsets.left + self.contentEdgeInsets.right;
}

- (void)adjustsVerticalCenterWithSpaceH:(CGFloat)spaceH correctX:(CGFloat)correctX
{
	CGSize	imageSize_	= [self imageForState:UIControlStateNormal].size;
    CGSize  titleSize_;
    if([kVersionOS intValue] >= 7)
    {
        titleSize_ = [[self titleForState:UIControlStateNormal] boundingRectWithSize:(CGSize){CGFLOAT_MAX, self.frameHeight} options:NSStringDrawingUsesDeviceMetrics attributes:@{NSFontAttributeName: self.titleLabel.font} context:nil].size;
    }
    else
    {
        titleSize_ = [[self titleForState:UIControlStateNormal] sizeWithFont:self.titleLabel.font forWidth:CGFLOAT_MAX lineBreakMode:self.titleLabel.lineBreakMode];
    }
    
	self.titleEdgeInsets	= UIEdgeInsetsMake(0, -imageSize_.width, -(imageSize_.height + spaceH), 0);
	self.imageEdgeInsets	= UIEdgeInsetsMake(-(titleSize_.height + spaceH), 0, 0, -(titleSize_.width + correctX));
}

- (void)adjustsVerticalCenterWithSpaceH:(CGFloat)spaceH
{
	[self adjustsVerticalCenterWithSpaceH:spaceH correctX:0.0f];
}

- (void)adjustsRight
{
	[self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
	
	CGSize	imageSize_	= [self imageForState:UIControlStateNormal].size;
	CGSize	titleSize_	= [[self titleForState:UIControlStateNormal] sizeWithFont:self.titleLabel.font forWidth:CGFLOAT_MAX lineBreakMode:self.titleLabel.lineBreakMode];
	
	self.titleEdgeInsets	= UIEdgeInsetsMake(0, 0, 0, imageSize_.width + 5);
	self.imageEdgeInsets	= UIEdgeInsetsMake(0, 0, 0, -titleSize_.width);
}

@end
