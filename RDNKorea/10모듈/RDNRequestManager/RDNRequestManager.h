//
//  RDNRequestManager.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 16..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RDNRequestDoneBlock)(BOOL success, id data, id extra);

@interface RDNRequestManager : NSObject
+ (void)requestSigninWithId:(NSString *)clienId password:(NSString *)password done:(RDNRequestDoneBlock)doneBlock;
@end
