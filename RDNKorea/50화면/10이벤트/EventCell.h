//
//  EventCell.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RDNEvent;

@interface EventCell : UITableViewCell
@property (nonatomic, strong) RDNEvent *event;
@end
