//
//  MenuCell.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 10..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "MenuCell.h"
#import "RDNMenuItem.h"

@interface MenuCell()
{
    __weak IBOutlet UILabel *titleLabel_;
}
@end


@implementation MenuCell

#pragma mark - setter
- (void)setItem:(RDNMenuItem *)item {
    _item = item;
    
    [titleLabel_ setText:_item.title];
}

@end
