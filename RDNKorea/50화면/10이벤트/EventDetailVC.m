//
//  EventDetailVC.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "EventDetailVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RDNEvent.h"
#import "RDNPoint.h"
#import "UIImageView+AFNetworking.h"

#define kGoogleApi          @"AIzaSyBGBg0C5Q9wF3Cr7kecvB7eeeURcLfv_UA"   // 구글 iOS SDK키
#define kWorldWeatherApiV2  @"9a9fdb297cf8660c587abaa8eaa96"
#define kWorldWeatherApiV1  @"exqfmxy2c8ay5ay3uzjst4xd"

@interface EventDetailVC () <GMSMapViewDelegate>
{
    GMSMapView *mapView_;
    GMSGeocoder *geoCoder_;
    
    __weak IBOutlet UIView *contentView_;       // mapView_의 영역을 셋팅하기 위해 IBOutlet으로 연결된 뷰
    __weak IBOutlet UIView *informationView_;   // 지도 하단 정보영역. 투명하게 해서 컨텐츠 위로 올릴지 정해야함
    __weak IBOutlet UIButton *trackButton_;
    __weak IBOutlet UILabel *addressLabel_;
    __weak IBOutlet UILabel *weatherLabel_;
    __weak IBOutlet UILabel *speedLabel_;
    
    NSString *administrativeArea_;              // 이게 바뀐경우에만 날씨 재검색
    CLLocationCoordinate2D previousCoord_;      // 속도측정용
    NSTimer *speedUpdateTimer_;                 // 속도측정용. 1초마다 GPS이동거리를 계산
}
@end

@implementation EventDetailVC

- (void)awakeFromNib {
    addressLabel_.text = @"";
    weatherLabel_.text = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.event.name;
    
    CLLocationCoordinate2D banmini = CLLocationCoordinate2DMake(37.513002, 127.00223);  // 반미니 기본좌표
    
    [GMSServices provideAPIKey:kGoogleApi];
    
    geoCoder_   = [GMSGeocoder geocoder];
    
    mapView_    = [GMSMapView mapWithFrame:contentView_.bounds
                                 camera:[GMSCameraPosition cameraWithTarget:banmini zoom:14]];
    mapView_.myLocationEnabled          = NO;
    mapView_.delegate                   = self;
    mapView_.settings.myLocationButton  = NO;
    mapView_.settings.compassButton     = NO;
    mapView_.mapType                    = kGMSTypeNormal;
    mapView_.trafficEnabled             = YES;
    [mapView_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [contentView_ addSubview:mapView_];
    
    [self requestMapItemDataWithURL:self.event.route
                               done:^(NSArray *waypoints, NSArray *paths) {
                                   [mapView_ clear];
                                   [self addMarkers:waypoints];
                                   [self addPaths:paths];
                               }];
    
    [self requestReverseGeocodingWithCoor:banmini];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [mapView_ addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [mapView_ removeObserver:self forKeyPath:@"myLocation" context:nil];
    [speedUpdateTimer_ invalidate];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

#pragma mark - Map
- (void)addMarkers:(NSArray *)array {
    for(RDNPoint *point in array) {
        GMSMarker *marker   = [[GMSMarker alloc] init];
        marker.position     = CLLocationCoordinate2DMake(point.latitude, point.longitude);
        marker.title        = point.name;
        marker.snippet      = point.name;
        marker.map          = mapView_;
    }
}

- (void)addPaths:(NSArray *)array {
    GMSMutablePath *path = [GMSMutablePath path];
    for(RDNPoint *point in array) {
        [path addLatitude:point.latitude longitude:point.longitude];
    }
    GMSPolyline *polyline   = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth    = 3;
    polyline.strokeColor    = [UIColor redColor];
    polyline.map = mapView_;
}

#pragma mark - GMSMapViewDelegate
- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    if(gesture) {
        if(trackButton_.selected) {
            [self onTrack]; // 사용자가 지도를 움직인 경우 트래킹 OFF
        }
    }
}

#pragma mark - ReverseGeocoding
- (void)requestReverseGeocodingWithCoor:(CLLocationCoordinate2D)coor {
    [geoCoder_ reverseGeocodeCoordinate:coor
                      completionHandler:^(GMSReverseGeocodeResponse *res, NSError *error) {
                          if(!error) {
                              GMSAddress *address = res.firstResult;
                              NSString *addr = [NSString stringWithFormat:@"%@ %@", address.locality, address.thoroughfare];
                              addr = [addr stringByReplacingOccurrencesOfString:@"," withString:@" "];
                              [addressLabel_ setText:addr];
                              
                              if(![administrativeArea_ isEqualToString:address.administrativeArea]) {   // 도시 바뀐경우 날씨 요청
                                  [self requestWeatherData];
                              }
                              
                              administrativeArea_ = address.administrativeArea;
                          }
                      }];
}

#pragma mark - Map Waypoint, Path
- (void)requestMapItemDataWithURL:(NSString *)url done:(void (^)(NSArray *waypoints, NSArray *paths))done {
    [RDNIndicator increase];
    NSMutableURLRequest *request = [NSMutableURLRequest tbxmlGetRequestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection tbxmlAsyncRequest:request
                               success:^(NSData *data, NSURLResponse *response) {
                                   [self parse:data done:^(NSArray *waypoints, NSArray *paths) {
                                       done(waypoints, paths);
                                       [RDNIndicator decrease];
                                   }];
                               } failure:^(NSData *data, NSError *error) {
//                                   [RDNToast showWithMessage:@"지도정보를 가져오지 못했습니다."];
                                   [RDNIndicator decrease];
                               }];
}

- (void)parse:(NSData *)data done:(void (^)(NSArray *waypoints, NSArray *paths))done {
    NSError *error;
    TBXML *xml = [TBXML newTBXMLWithXMLData:data error:&error];
    TBXMLElement *root = [xml rootXMLElement];
    
    // Waypoint Parsing
    NSMutableArray *waypoints = [NSMutableArray array];
    TBXMLElement *wpt  = [TBXML childElementNamed:@"wpt" parentElement:root error:&error];
    while (wpt) {
        NSString *lat = [TBXML valueOfAttributeNamed:@"lat" forElement:wpt];
        NSString *lon = [TBXML valueOfAttributeNamed:@"lon" forElement:wpt];
        TBXMLElement *wpt_name = [TBXML childElementNamed:@"name" parentElement:wpt error:&error];
        NSString *name = [TBXML textForElement:wpt_name];
//        TBXMLElement *wpt_cmt  = [TBXML childElementNamed:@"cmt" parentElement:wpt error:&error];
//        NSString *cmt = [TBXML textForElement:wpt_cmt];
        
        RDNPoint *point = [[RDNPoint alloc] init];
        point.name      = [name copy];
//        point.desc      = [cmt copy];
        point.latitude  = [lat doubleValue];
        point.longitude = [lon doubleValue];
        [waypoints addObject:point];
        
        wpt = [TBXML nextSiblingNamed:@"wpt" searchFromElement:wpt];
    }
    
    // 루트 파싱
    NSMutableArray *routes = [NSMutableArray array];
    TBXMLElement *trk = [TBXML childElementNamed:@"trk" parentElement:root];
    TBXMLElement *trkseg = [TBXML childElementNamed:@"trkseg" parentElement:trk];
    TBXMLElement *trkpt = [TBXML childElementNamed:@"trkpt" parentElement:trkseg];
    while (trkpt) {
        NSString *lat = [TBXML valueOfAttributeNamed:@"lat" forElement:trkpt];
        NSString *lon = [TBXML valueOfAttributeNamed:@"lon" forElement:trkpt];
        
        RDNPoint *point = [[RDNPoint alloc] init];
        point.latitude  = [lat doubleValue];
        point.longitude = [lon doubleValue];
        [routes addObject:point];
        
        trkpt = [TBXML nextSiblingNamed:@"trkpt" searchFromElement:trkpt];
    }
    
    done(waypoints, routes);
}

#pragma mark - weather API
- (void)requestWeatherData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString *requestURL = [NSString stringWithFormat:@"http://api.worldweatheronline.com/free/v2/weather.ashx?q=%@,%@&format=json&date=today&fx=no&key=%@", @(mapView_.camera.target.latitude), @(mapView_.camera.target.longitude), kWorldWeatherApiV2];
        NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:requestURL]];
        NSDictionary *dic       = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSDictionary *dataDic   = [dic objectForKey:@"data"];
        NSArray *conditions     = [dataDic objectForKey:@"current_condition"];
        if(conditions.count == 0) {
            [weatherLabel_ setText:@"날씨정보를 가져올 수 없습니다."];
            return ;
        }
        
        NSDictionary *current   = [conditions objectAtIndex:0];
        NSString *feelsLikeC    = [current objectForKey:@"FeelsLikeC"];     // 체감온도
        NSString *tempC         = [current objectForKey:@"temp_C"];         // 현재온도
        NSString *humidity      = [current objectForKey:@"humidity"];       // 습도
        NSString *directionW    = [current objectForKey:@"winddir16Point"]; // 풍향
        NSString *windspeed     = [current objectForKey:@"windspeedKmph"];  // 풍속
        NSString *icon          = @"";                                      // 날씨아이콘
        NSArray *icons          = [current objectForKey:@"weatherIconUrl"];
        if(icons.count > 0) {
            icon = [[icons objectAtIndex:0] objectForKey:@"value"];
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSString *weather = [NSString stringWithFormat:@"현재 %@°C, 체감 %@°C, 습도 %@%%, 바람 %@(%@km/h)", tempC, feelsLikeC, humidity, directionW, windspeed];
            [weatherLabel_ setText:weather];
        });
    });
}

#pragma mark - Event
- (IBAction)onTrack {
    [trackButton_ setSelected:!trackButton_.selected];
    mapView_.myLocationEnabled = trackButton_.selected;
    [speedLabel_ setAlpha:trackButton_.selected ? 1.0 : 0.4];
    [[UIApplication sharedApplication] setIdleTimerDisabled:trackButton_.selected];
    
    if(trackButton_.selected) {
        speedUpdateTimer_ = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateSpeedLabel)
                                                userInfo:nil
                                                 repeats:YES];
    }
    else {
        [speedUpdateTimer_ invalidate];
        previousCoord_ = CLLocationCoordinate2DMake(0, 0);
        [speedLabel_ setText:@"0"];
    }
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        if(trackButton_.selected) { // 트랙중일때만 내 위치에 따라 지도 이동
            [mapView_ animateToLocation:mapView_.myLocation.coordinate];
            [mapView_ animateToZoom:15];
            [self requestReverseGeocodingWithCoor:mapView_.myLocation.coordinate];
        }
    }
}

#pragma mark - Private
- (double)distanceFrom:(CLLocationCoordinate2D)from to:(CLLocationCoordinate2D)to {
    double d2r  = M_PI / 180.0;
    double dlon = (to.longitude - from.longitude) * d2r;
    double dlat = (to.latitude - from.latitude) * d2r;
    double a    = pow(sin(dlat/2.0), 2) + cos(from.latitude * d2r) * cos(to.latitude * d2r) * pow(sin(dlon/2.0) , 2);
    double c    = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d    = 6367 * c;
    
    return d;
}

- (void)updateSpeedLabel {
    if(previousCoord_.longitude != 0) {
        double km       = [self distanceFrom:previousCoord_ to:mapView_.myLocation.coordinate];
        double speed    = km * 60.0 * 60.0; // km/h
        NSInteger speedInteger = (NSInteger)speed;
        [speedLabel_ setText:[NSString stringWithFormat:@"%@", @(speedInteger)]];
    }
    previousCoord_ = mapView_.myLocation.coordinate;
}
@end
